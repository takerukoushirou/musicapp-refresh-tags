//@osa-lang:JavaScript
/*
 * Music.app Reload File Tags
 *
 * Copyright (C) 2022, Michael Maier
 */

'use strict';

// Run event, invoked on script invocation.
var run = (() => {

    const appletName = "Reload File Tags";

    /**
     * Represents a duration with human-readable formatting.
     */
    class Duration {
        #_hours;
        #_minutes;
        #_seconds;

        /**
         * Initialises a new Duration instance.
         * @param ms
         * Duration in milliseconds.
         */
        constructor(ms) {
            let s = Math.floor(ms / 1000);
            let m = Math.floor(s / 60);
            let h = Math.floor(m / 60);

            this._hours = h;
            this._minutes = m - (h * 60);
            this._seconds = s - (m * 60);
        }

        /**
         * Duration in hours.
         */
        get hours() {
            return this._hours;
        }

        /**
         * Remainder duration in minutes after hours.
         */
        get minutes() {
            return this._minutes;
        }

        /**
         * Remainder duration in seconds after minutes.
         */
        get seconds() {
            return this._seconds;
        }

        /**
         * Formats duration as a human-readable string.
         * @returns
         * Formatted duration string with hours, minutes and seconds as needed.
         */
        toString() {
            let components = [];

            if (this._hours > 0) {
                components.push(
                    this._hours.toLocaleString() +
                    (this._hours === 1 ? " hour" : " hours")
                );
            }

            if (!isNaN(this._minutes) &&
                (components.length > 0 || this._minutes > 0)) {
                components.push(
                    this._minutes.toLocaleString() +
                    (this._minutes === 1 ? " minute" : " minutes")
                );
            }

            if (!isNaN(this._seconds)) {
                if (this._seconds <= 0) {
                    components.push("less than 1 second");
                }
                else {
                    components.push(
                        this._seconds.toLocaleString() +
                        (this._seconds === 1 ? " second" : " seconds")
                    );
                }
            }

            return components.join(" ");
        }
    }

    /**
     * Progress metering helper to keep track of progress and compute estimates.
     * Time is immediately recorded after a new instance was created.
     */
    class ProgressMeter {
        /**
         * Number of completed units. Update value to keep track of progress.
         * Should never exceed the total number of units and must be positive.
         */
        completedUnits = 0;

        #_stopwatch;
        #_totalUnits;

        constructor(totalUnits) {
            this._stopwatch = new Stopwatch();

            if (typeof totalUnits !== "undefined") {
                this.totalUnits = totalUnits;
            }
        }

        /**
         * Duration of metered progress.
         */
        get completedDuration() {
            return this._stopwatch.duration;
        }

        /**
         * Number of completed units as a localized string.
         */
        get completedUnitsLocalized() {
            return this.completedUnits.toLocaleString();
        }

        /**
         * Percentage of completed units.
         */
        get completedUnitsRatio() {
            return this.completedUnits / this._totalUnits;
        }

        /**
         * Duration estimate until completion based on current progress.
         */
        get estimatedRemainingDuration() {
            return new Duration(
                Math.ceil(
                    this._stopwatch.durationInMs
                    / this.completedUnits
                    * this.remainingUnits
                )
            );
        }

        /**
         * Number of next work unit as a localized string.
         */
        get nextUnitLocalized() {
            return (this.completedUnits + 1).toLocaleString();
        }

        /**
         * Number of remaining units.
         */
        get remainingUnits() {
            return this._totalUnits - this.completedUnits;
        }

        /**
         * Number of remaining units as a localized string.
         */
        get remainingUnitsLocalized() {
            return this.remainingUnits.toLocaleString();
        }

        /**
         * Percentage of remaining units.
         */
        get remainingUnitsRatio() {
            return this.remainingUnits / this._totalUnits;
        }

        /**
         * Number of total units.
         */
        get totalUnits() {
            return this._totalUnits;
        }

        /**
         * Number of total units as a localized string.
         */
        get totalUnitsLocalized() {
            return this._totalUnits.toLocaleString();
        }

        /**
         * Sets the total number of units.
         * @param {number} value
         * New value for total number of units. Must be greater than zero.
         */
        set totalUnits(value) {
            if (value <= 0) {
                throw new Error("Number of total units must be greater than 0.");
            }

            this._totalUnits = value;
        }
    }

    /**
     * Measures time with support for markers. A stopwatch object is immediately
     * active after instantiation.
     */
    class Stopwatch {
        #startDate;
        #lastMarkDate;

        constructor() {
            this.startDate = new Date();
        }

        /**
         * Returns the date of the last marker.
         */
        get lastMark() {
            return this.lastMarkDate ?? this.startDate;
        }

        /**
         * Returns the duration since the last marker in milliseconds.
         */
        get lastMarkDurationInMs() {
            return new Date() - this.lastMark;
        }

        /**
         * Sets a new marker and returns the duration since the last marker.
         * @returns
         * Duration since last marker or start of the stopwatch, if no marker
         * has been created yet.
         */
        mark() {
            return new Duration(this.mark());
        }

        /**
         * Sets a new marker and returns the duration since the last marker.
         * @returns
         * Duration in milliseconds since last marker or start of the stopwatch,
         * if no marker has been created yet.
         */
        markInMs() {
            let mark = new Date();
            let duration = mark - this.lastMark;

            this.lastMarkDate = mark;

            return duration;
        }

        /**
         * Sets a new marker and returns the duration since the last marker in
         * seconds (with precision 2).
         * @returns
         * Duration in seconds since last marker or start of the stopwatch, if
         * no marker has been created yet.
         */
        markInSeconds() {
            return (this.mark() / 1000).toPrecision(2);
        }

        /**
         * Resets the start date to now.
         * @returns
         * Reference to this instance.
         */
        reset() {
            this.startDate = new Date();
            this.lastMarkDate = null;

            return this;
        }

        /**
         * Gets the duration between now and the start date of the stopwatch.
         */
        get duration() {
            return new Duration(this.durationInMs);
        }

        /**
         * Gets the duration between now and the start date of the stopwatch in
         * milliseconds.
         */
        get durationInMs() {
            return new Date() - this.startDate;
        }

        /**
         * Gets the duration between now and the start date of the stopwatch in
         * seconds (with precision 2).
         */
        get durationInSeconds() {
            return (this.duration / 1000).toPrecision(2);
        }
    }


    /**
     * Represents a music track.
     *
     * Bridges access to an application track object with implicit caching of
     * loaded properties to speed up further access, e.g. during a sort operation.
     */
    class Track {
        #_item;
        #_album;
        #_albumArtist;
        #_artist;
        #_bitRate;
        #_discNumber;
        #_kind;
        #_name;
        #_sampleRate;
        #_sortAlbum;
        #_sortAlbumArtist;
        #_sortArtist;
        #_sortByArtist;
        #_sortByAlbum;
        #_trackNumber;

        /**
         * Initialises a new Track instance from an application track object.
         * @param item
         * Application track object to read and cache data from.
         */
        constructor(item) {
            this._item = item;
        }

        get album() {
            return this._album ?? (this._album = this._item.album());
        }

        get albumArtist() {
            return this._albumArtist ?? (this._albumArtist = this._item.albumArtist());
        }

        get artist() {
            return this._artist ?? (this._artist = this._item.artist());
        }

        /**
         * Numeric bitrate of track in kbps.
         */
        get bitRate() {
            return this._bitRate ?? (this._bitRate = this._item.bitRate());
        }

        get discNumber() {
            return this._discNumber ?? (this._discNumber = this._item.discNumber());
        }

        get kind() {
            return this._kind ?? (this._kind = this._item.kind());
        }

        get name() {
            return this._name ?? (this._name = this._item.name());
        }

        /**
         * Numeric sample rate of track in Hz.
         */
        get sampleRate() {
            return this._sampleRate ?? (this._sampleRate = this._item.sampleRate());
        }

        get sortAlbum() {
            return this._sortAlbum ?? (this._sortAlbum = this._item.sortAlbum());
        }

        get sortAlbumArtist() {
            return this._sortAlbumArtist ?? (this._sortAlbumArtist = this._item.sortAlbumArtist());
        }

        get sortArtist() {
            return this._sortArtist ?? (this._sortArtist = this._item.sortArtist());
        }

        /**
         * Transformed artist name used for sorting tracks.
         */
        get sortByArtist() {
            if (typeof this._sortByArtist === "undefined") {
                let artist = this.sortAlbumArtist;

                if (!artist) {
                    artist = this.albumArtist;

                    if (!artist) {
                        artist = this.artist;
                    }
                }

                this._sortByArtist = artist.toUpperCase();
            }

            return this._sortByArtist;
        }

        /**
         * Transformed album name used for sorting tracks.
         */
        get sortByAlbum() {
            if (typeof this._sortByAlbum === "undefined") {
                let album = this.sortAlbum;

                if (!album) {
                    album = this.album;
                }

                this._sortByAlbum = album.toUpperCase();
            }

            return this._sortByAlbum;
        }

        get trackNumber() {
            return this._trackNumber ?? (this._trackNumber = this._item.trackNumber());
        }

        refresh() {
            return this._item.refresh();
        }

        /**
         * Compares two tracks for sorting. Designed to be used in Array.sort().
         */
        static compare(a, b) {
            const artistComparison = a.sortByArtist.localeCompare(b.sortByArtist);

            if (artistComparison == 0) {
                const albumComparison = a.sortByAlbum.localeCompare(b.sortByAlbum);

                if (albumComparison == 0) {
                    const idA = a.discNumber * 10000 + a.trackNumber;
                    const idB = b.discNumber * 10000 + b.trackNumber;

                    return idA - idB;
                }

                return albumComparison;
            }

            return artistComparison;
        }
    }

    /**
     * Startup class, wraps the script main function and helpers.
     */
    class Startup {
        #app;
        #appProperties;

        constructor() {
            this.app = Application.currentApplication();
            this.app.includeStandardAdditions = true;

            // Cache properties, as direct access to properties yields undefined (JXA shortcoming).
            // If the script is not hosted, access to properties() or other properties will throw
            // a "Message not understood" error; in this case, a placeholder properties object will
            // be used instead.
            try {
                this.appProperties = this.app.properties();
            }
            catch (e) {
                // Running as a standalone applet, use static properties.
                this.appProperties = { name: appletName };
            }
        }

        /**
         * Name of the currently running application (applet or host).
         */
        get appName() {
            return this.appProperties["name"];
        }

        /**
         * Version of the currently running application (applet or host).
         */
        get appVersion() {
            return this.appProperties["version"];
        }

        /**
         * Retrieves the Music application instance. This does not launch the
         * application, if it is not already running.
         */
        get musicApp() {
            if (this.runningInsideMusic) {
                // Use current application instance if already running within
                // Music, as otherwise the host application will be indefinitely
                // blocked.
                return this.app;
            }

            return Application("Music");
        }

        /**
         * Determines whether the script is hosted within Music.app.
         */
        get runningInsideMusic() {
            return this.appName === "Music";
        }

        /**
         * Fetches tracks from Music.app.
         * @returns
         * Array of Track instances on success, otherwise undefined.
         */
        fetchTracks() {
            const music = this.musicApp;

            if (!music.running()) {
                try {
                    this.app.displayAlert("Launch Music application?", {
                        as: "informational",
                        message: "Music is currently not running, but is required to access music tracks.",
                        buttons: ["Launch Music", "Cancel"],
                        defaultbutton: 1,
                        cancelButton: 2
                    });
                }
                catch {
                    // Use cancelled dialog, return undefined tracklist.
                    return;
                }

                // Music.app is automatically launched during first interaction.
            }

            let selectedTracks = music.selection();

            if (!this.runningInsideMusic && !selectedTracks.length) {
                // Only default to full tracklist if called as an applet.
                selectedTracks = music.tracks();
            }

            const tracks = [];

            for (let track of selectedTracks) {
                if (track.class() === 'fileTrack') {
                    tracks.push(new Track(track));
                }
            }

            return tracks;
        }

        /**
         * Main application.
         */
        main() {
            const stopwatch = new Stopwatch();

            // Retrieve array of tracks to process.
            Progress.totalUnitCount = -1;
            Progress.description = "Loading tracks...";
            console.log(`⏳ Getting track list from Music.app...`);
            Progress.additionalDescription = "This may take a while for larger libraries.";
            const tracks = this.fetchTracks();

            if (typeof tracks === "undefined") {
                console.log(`🛑 Cancelled.`);
                return;
            }

            const totalTracks = tracks.length;
            console.log(`✅ Found ${totalTracks} tracks in ${stopwatch.markInMs()} ms.`);
            Progress.totalUnitCount = 0;
            Progress.additionalDescription = undefined;

            // Sort tracks by album.
            Progress.totalUnitCount = -1;
            Progress.description = `Sorting ${totalTracks.toLocaleString()} tracks...`;
            Progress.additionalDescription = "This may take several minutes for larger libraries.";
            console.log(`⏳ Sorting tracks by album...`);
            tracks.sort(Track.compare);
            console.log(`✅ Sorted tracks in ${stopwatch.markInMs()} ms.`);
            Progress.totalUnitCount = 0;
            Progress.additionalDescription = undefined;

            // Refresh tracks in sorted order.
            let progressMeter = new ProgressMeter(totalTracks);
            let currentAlbum = null;

            Progress.totalUnitCount = totalTracks;
            Progress.completedUnitCount = 0;
            Progress.description = "Reloading tags from files...";

            let progressUpdate = (() => {
                let lastUpdate = new Date();

                return () => {
                    let now = new Date();

                    if (now - lastUpdate > 5000) {
                        console.log(`${(progressMeter.completedUnitsRatio * 100).toPrecision(2)}% completed`);
                        lastUpdate = now;
                    }
                };
            })();

            for (let track of tracks) {
                let trackAlbum = `${track.sortByArtist} – ${track.sortByAlbum}`;

                if (currentAlbum !== trackAlbum) {
                    console.log(`💿 ${trackAlbum}`);
                    currentAlbum = trackAlbum;
                }

                Progress.additionalDescription =
                    `Track ${progressMeter.nextUnitLocalized} of ${progressMeter.totalUnitsLocalized}:` +
                    ` ${trackAlbum} ${track.discNumber}-${track.trackNumber}`;
                console.log(
                    `   🎵 ${track.discNumber}-${track.trackNumber}. ${track.artist} – ${track.name}` +
                    `  (${track.kind}, ${track.sampleRate} Hz, ${track.bitRate} kbps)`
                );

                track.refresh();

                progressMeter.completedUnits++;
                Progress.completedUnitCount = progressMeter.completedUnits;
                progressUpdate();

                // Update estimate.
                Progress.description = `Reloading tags from files: ${progressMeter.completedDuration} passed, ${progressMeter.estimatedRemainingDuration} until completion.`;
            }

            const refreshTime = progressMeter.completedDuration;
            const totalTime = stopwatch.duration;
            console.log();
            console.log(`✅ Finished processing ${totalTracks} tracks in ${totalTime} (refresh took ${refreshTime}).`);

            this.app.displayNotification(
                `Refresh took ${refreshTime}, total time ${totalTime}.`, {
                withTitle: appletName,
                subtitle: `Processed ${totalTracks} music tracks`
            });
        }
    }

    return () => {
        try {
            return new Startup().main();
        }
        catch (e) {
            var app = Application.currentApplication();
            app.includeStandardAdditions = true;

            app.displayAlert(`${appletName} Error`, { message: `An unhandled error ocurred: ${e.message}` });

            throw e;
        }
    };
})();